package by.kolovaitis

import by.kolovaitis.files.FileLoader
import by.kolovaitis.likes.LikesController
import by.kolovaitis.likes.LikesControllerDB
import by.kolovaitis.messages.MessagesController
import by.kolovaitis.messages.MessagesControllerDB
import by.kolovaitis.pairs.PairsController
import by.kolovaitis.pairs.PairsControllerDB
import by.kolovaitis.users.NotInfoException
import by.kolovaitis.users.UsersController
import by.kolovaitis.users.UsersControllerDB
import com.auth0.jwt.*
import com.auth0.jwt.algorithms.*
import com.fasterxml.jackson.databind.*
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.*
import java.io.FileNotFoundException

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun initDB() {
    Class.forName("org.postgresql.Driver")
    var dbUrl = System.getenv("JDBC_DATABASE_URL")
    if (dbUrl == null) {
        dbUrl = "jdbc:postgresql://localhost:5432/testdb?user=postgres&password=password"
    }
    Class.forName("org.postgresql.Driver")
    Database.connect(
        dbUrl, driver = "org.postgresql.Driver"
    )
}

fun Application.module() {
    val simpleJwt = SimpleJWT("my-super-secret-for-jwt")
    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        allowCredentials = true
        anyHost()
    }
    install(StatusPages) {
        exception<InvalidCredentialsException> { exception ->
            call.respond(HttpStatusCode.Unauthorized, mapOf("OK" to false, "error" to (exception.message ?: "")))
        }
        exception<AlreadyInUseException> { exception ->
            call.respond(HttpStatusCode.Unauthorized, mapOf("OK" to false, "error" to (exception.message ?: "")))
        }
        exception<FileNotFoundException> { exception ->
            call.respond(HttpStatusCode.NotFound, mapOf("OK" to false, "error" to (exception.message ?: "")))
        }
        exception<NotInfoException> { exception ->
            call.respond(HttpStatusCode.NotFound, mapOf("OK" to false, "error" to (exception.message ?: "")))
        }
        exception<Exception> { exception ->
            call.respond(HttpStatusCode.InternalServerError, mapOf("OK" to false, "error" to (exception ?: "")))

        }
    }
    install(Authentication) {
        jwt {
            verifier(simpleJwt.verifier)
            validate {
                UserIdPrincipal(it.payload.getClaim("name").asString())
            }
        }
    }
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT) // Pretty Prints the JSON
        }
    }
    initDB()
    val usersController: UsersController = UsersControllerDB()
    val messagesController: MessagesController = MessagesControllerDB()
    val pairsController: PairsController = PairsControllerDB()
    val likesController: LikesController = LikesControllerDB()
    routing {
        post("/login") {
            val post = call.receive<Login>()
            val user = User(post.login, post.password)
            if (!usersController.isCorrect(user)) {
                throw InvalidCredentialsException("Invalid credentials")
            } else {
                call.respond(mapOf("token" to simpleJwt.sign(user.login)))
            }
        }
        post("/register") {
            val post = call.receive<Register>()
            val user = User(post.login, post.password, post.name)
            if (!usersController.isExist(user)) {
                usersController.create(user)
                call.respond(mapOf("token" to simpleJwt.sign(user.login)))
            } else throw AlreadyInUseException("login already in use")
        }
        route("/image") {
            authenticate {
                post {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    val stream = call.receiveStream()
                    val bytes = stream.readAllBytes()
                    FileLoader.loadFile("$login.${call.request.contentType().contentSubtype}", bytes)
                    call.respond(mapOf("OK" to true))
                }
                get {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    call.respondFile(FileLoader.getFile(login))
                }
            }

        }
        route("/image/{login}") {
            get {
                val login = call.parameters["login"] ?:  ""
                call.respondFile(FileLoader.getFile(login))
            }
        }
        route("/location") {
            authenticate {
                post {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    val coordinates = call.receive<Pair<String, String>>()
                    usersController.setCoordinates(login, coordinates.first, coordinates.second)
                    call.respond(mapOf("OK" to true))
                }

                get {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    val credentials = usersController.userCredentials(login)
                    call.respond(mapOf(Pair(credentials.cord1, credentials.cord2)))
                }
            }
        }
        get("/location/{login}") {
            val login = call.parameters["login"] ?: ""
            val credentials = usersController.userCredentials(login)
            call.respond(mapOf(Pair(credentials.cord1, credentials.cord2)))
        }
        route("/usersFor") {
            authenticate {
                get {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    call.respond(mapOf("users" to usersController.allUsersFor(login)))
                }
            }
        }
        route("/credentials/{login}") {
            get {
                val login = call.parameters["login"] ?: ""
                call.respond(mapOf("credentials" to usersController.userCredentials(login)))
            }
        }
        route("/credentials") {
            authenticate {
                get {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    call.respond(mapOf("credentials" to usersController.userCredentials(login)))
                }
            }
        }
        route("/like/{login}") {
            authenticate {
                post {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    val loginAnket = call.parameters["login"] ?: ""
                    likesController.like(login, loginAnket, pairsController)
                    usersController.watch(login, loginAnket)
                    call.respond(mapOf("OK" to true))
                }
            }
        }
        route("/likes") {
            authenticate {
                get {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    likesController.likesFor(login)
                    call.respond(mapOf("likes" to likesController.likesFor(login)))
                }
            }
        }
        route("/pair/{login}") {
            authenticate {
                post {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    val login2 = call.parameters["login"] ?: ""
                    pairsController.createPair(login, login2)
                    call.respond(mapOf("OK" to true))
                }
            }
        }
        route("/pairs") {
            authenticate {
                get {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    call.respond(mapOf("pairs" to pairsController.pairsFor(login)))
                }
            }
        }
        route("/decline/{login}") {
            authenticate {
                post {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    val login2 = call.parameters["login"] ?: ""
                    likesController.decline(login2, login)
                    call.respond(mapOf("likes" to likesController.likesFor(login)))
                }
            }
        }
        route("/watch/{login}") {
            authenticate {
                post {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    val loginAnket = call.parameters["login"] ?: ""
                    usersController.watch(login, loginAnket)
                    call.respond(mapOf("OK" to true))
                }
            }
        }
        route("/message/{login}") {
            authenticate {
                post {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    val login2 = call.parameters["login"] ?: ""

                    call.respond(
                        mapOf(
                            "messages" to listOf<by.kolovaitis.messages.Message>(messagesController.addMessage(login, login2, call.receive<Message>().text))
                        )
                    )                }
            }
        }
        route("/read/{id}") {
            authenticate {
                post {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    val id = call.parameters["id"] ?: ""
                    messagesController.readMessage(id.toInt())
                    call.respond(mapOf("OK" to true))
                }
            }
        }
        route("/new_messages") {
            authenticate {
                get {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    call.respond(mapOf("messages" to messagesController.allNewMessages(login)))
                }
            }
        }

        route("/messages/{login}") {
            authenticate {
                get {
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal")
                    val login = principal.name
                    val login2 = call.parameters["login"] ?: ""
                    call.respond(
                        mapOf(
                            "messages" to messagesController.allMessagesWith(login, login2)
                        )
                    )
                }
            }
        }
    }
}

open class SimpleJWT(val secret: String) {
    private val algorithm = Algorithm.HMAC256(secret)
    val verifier = JWT.require(algorithm).build()
    fun sign(name: String): String = JWT.create().withClaim("name", name).sign(algorithm)
}

class User(val login: String, val password: String? = null, val name: String? = null)

class InvalidCredentialsException(message: String) : RuntimeException(message)
class AlreadyInUseException(message: String) : RuntimeException(message)

class Login(val login: String, val password: String)
class Register(val login: String, val password: String, val name: String)

class Message(val text: String)