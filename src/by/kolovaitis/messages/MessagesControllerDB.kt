package by.kolovaitis.messages

import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class MessagesControllerDB : MessagesController {
    override fun allMessagesWith(login: String, opponent: String): List<Message> {
        val messages = mutableListOf<Message>()
        messages.addAll(transaction {
            Messages.select { Messages.receiver eq login and (Messages.sender eq opponent)}.map {
                Message(
                    it[Messages.id],
                    it[Messages.sender], it[Messages.receiver], it[Messages.text], it[Messages.watched]
                )
            }
        })
        messages.addAll(transaction {
            Messages.select { Messages.sender eq login and (Messages.receiver eq opponent)}.map {
                Message(
                    it[Messages.id],
                    it[Messages.sender], it[Messages.receiver], it[Messages.text], it[Messages.watched]
                )
            }
        })
        return messages

    }

    override fun allNewMessages(login: String): List<Message> {
        return transaction {
            Messages.select { Messages.watched eq "false" and (Messages.receiver eq login) }.map {
                Message(
                    it[Messages.id],
                    it[Messages.sender], it[Messages.receiver], it[Messages.text], it[Messages.watched]
                )
            }
        }
    }

    override fun readMessage(messageId: Int) {
        transaction {
            Messages.update({ Messages.id eq messageId }) {
                it[Messages.watched] = true
            }
        }
    }

    override fun addMessage(from: String, to: String, text: String): Message {
        return Message(transaction {
            Messages.insert {
                it[Messages.receiver] = to
                it[Messages.sender] = from
                it[Messages.text] = text
                it[Messages.watched] = false
            }
        }.generatedKey!!.toInt(), from, to, text, false)
    }
}