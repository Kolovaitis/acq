package by.kolovaitis.messages

import by.kolovaitis.likes.Likes
import by.kolovaitis.likes.Likes.autoIncrement
import by.kolovaitis.likes.Likes.primaryKey
import org.jetbrains.exposed.sql.Table

object Messages: Table() {
    val id = Messages.integer("id").primaryKey().autoIncrement()
    val sender = Messages.text("sender")
    val receiver = Messages.text("receiver")
    val text = Messages.text("text")
    val watched = Messages.bool("watched")
}