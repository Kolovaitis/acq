package by.kolovaitis.messages

interface MessagesController {
    fun allMessagesWith(login:String, opponent:String):List<Message>
    fun allNewMessages(login:String):List<Message>
    fun readMessage(messageId:Int)
    fun addMessage(from:String, to:String, text:String):Message
}