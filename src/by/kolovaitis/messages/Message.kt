package by.kolovaitis.messages

data class Message(val id:Int, val from:String, val to:String, val text:String, var watched:Boolean) {}