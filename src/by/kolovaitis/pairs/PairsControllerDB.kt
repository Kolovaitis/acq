package by.kolovaitis.pairs

import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class PairsControllerDB : PairsController {
    override fun pairsFor(login: String): List<String> {
        val pairs = mutableListOf<String>()
        pairs.addAll(transaction {
            Pairs.select { Pairs.login1 eq login }.map { it[Pairs.login2] }
        })
        pairs.addAll(transaction {
            Pairs.select { Pairs.login2 eq login }.map { it[Pairs.login1] }
        })
        return pairs
    }

    override fun createPair(login1: String, login2: String) {
        transaction {
            Pairs.insert {
                it[Pairs.login1] = login1
                it[Pairs.login2] = login2
            }
        }
    }
}