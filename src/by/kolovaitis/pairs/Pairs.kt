package by.kolovaitis.pairs

import by.kolovaitis.users.Users
import by.kolovaitis.users.Watched
import by.kolovaitis.users.Watched.autoIncrement
import by.kolovaitis.users.Watched.primaryKey
import org.jetbrains.exposed.sql.Table

object Pairs:Table() {
    val id= Pairs.integer("id").primaryKey().autoIncrement()
    val login1 = Pairs.text("login1")
    val login2 = Pairs.text("login2")
}