package by.kolovaitis.pairs

interface PairsController {
    fun pairsFor(login:String):List<String>
    fun createPair(login1:String, login2:String)
}