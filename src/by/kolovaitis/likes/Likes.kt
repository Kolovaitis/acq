package by.kolovaitis.likes

import org.jetbrains.exposed.sql.Table

object Likes : Table() {
    val id = Likes.integer("id").primaryKey().autoIncrement()
    val login1 = Likes.text("login1")
    val login2 = Likes.text("login2")
}