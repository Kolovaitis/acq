package by.kolovaitis.likes

import by.kolovaitis.pairs.PairsController
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.and

class LikesControllerDB : LikesController {
    override fun like(sender: String, recipient: String, pairsController: PairsController) {
        transaction {
            Likes.insert {
                it[Likes.login1] = sender
                it[Likes.login2] = recipient
            }
        }
        if (transaction {
                Likes.select { Likes.login2 eq sender and (Likes.login1 eq recipient) }.map { it[Likes.id] }
            }.isNotEmpty()) {
            pairsController.createPair(sender, recipient)
        }
    }

    override fun likesFor(login: String): List<String> {
        return transaction {
            Likes.select { Likes.login2 eq login }.map { it[Likes.login1] }
        }
    }

    override fun decline(sender: String, recipient: String) {
        transaction {
            Likes.deleteWhere { Likes.login1 eq sender and (Likes.login2 eq recipient) }
        }
    }
}