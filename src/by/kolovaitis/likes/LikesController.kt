package by.kolovaitis.likes

import by.kolovaitis.pairs.PairsController

interface LikesController {
    fun like(sender:String, recipient:String, pairsController: PairsController)
    fun likesFor(login:String):List<String>
    fun decline(sender: String, recipient: String)
}