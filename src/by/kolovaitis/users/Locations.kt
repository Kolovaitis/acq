package by.kolovaitis.users

import org.jetbrains.exposed.sql.Table

object Locations:Table() {
    val login = Locations.text("login").primaryKey()
    val cord1 = Locations.text("cord1")
    val cord2 = Locations.text("cord2")
}