package by.kolovaitis.users

import by.kolovaitis.User

interface UsersController {
    fun create(user: User)
    fun isExist(user: User):Boolean
    fun isCorrect(user:User):Boolean
    fun allUsers():List<User>
    fun allUsersFor(login:String):List<String>
    fun userCredentials(login:String):UserCredentials
    fun watch(login:String, loginAnket:String)
    fun setCoordinates(login:String, cord1:String, cord2:String)
}
