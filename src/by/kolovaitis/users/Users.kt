package by.kolovaitis.users

import org.jetbrains.exposed.sql.Table

object Users: Table() {
    val login = text("login").primaryKey()
    val password = text("password")
    val name = text("name")
}
