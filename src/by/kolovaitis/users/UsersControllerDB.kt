package by.kolovaitis.users

import by.kolovaitis.User
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class UsersControllerDB : UsersController {
    override fun create(user: User) {
        transaction {
            Users.insert {
                it[Users.login] = user.login
                it[Users.name] = user.name!!
                it[Users.password] = user.password!!
            }
        }
    }

    override fun isExist(user: User): Boolean {
        return transaction {
            Users.select(Users.login eq user.login).count()
        } > 0
    }

    override fun isCorrect(user: User): Boolean {
        val userFromDB = transaction {
            Users.select(Users.login eq user.login).map { User(login = it[Users.login], password = it[Users.password]) }
        }.firstOrNull()
        return if (userFromDB != null) {
            user.password == userFromDB.password
        } else {
            false
        }
    }

    override fun allUsers(): List<User> {
        val users: ArrayList<User> = arrayListOf()
        transaction {
            Users.selectAll().map { users.add(User(login = it[Users.login], name = it[Users.login])) }
        }
        return users
    }

    override fun allUsersFor(login: String): List<String> {
        val usersLoginsToExclude = mutableListOf<String>(login)
        transaction {
            Watched.select { Watched.login eq login }.map { usersLoginsToExclude.add(it[Watched.login_anket]) }
        }
        val allLogins = transaction {
            Users.selectAll().map { it[Users.login] }
        }.toMutableList()
        allLogins.removeAll(usersLoginsToExclude)
        return allLogins
    }

    override fun userCredentials(login: String): UserCredentials {
        val location = transaction {
            Locations.select { Locations.login eq login }.map { Pair(it[Locations.cord1], it[Locations.cord2]) }
        }.firstOrNull()
        val name = transaction {
            Users.select { Users.login eq login }.map { it[Users.name] }
        }.firstOrNull()
        if (location == null && name == null) {
            throw NotInfoException("Not enough info")
        }
        return UserCredentials(login, name!!, location!!.first, location!!.second)
    }

    override fun watch(login: String, loginAnket: String) {
        transaction {
            Watched.insert {
                it[Watched.login] = login
                it[Watched.login_anket] = loginAnket
            }
        }
    }

    override fun setCoordinates(login: String, cord1: String, cord2: String) {
        val oldCoordinates = transaction {
            Locations.select { Locations.login eq login }.map { Pair(it[Locations.cord1], it[Locations.cord2]) }
        }.firstOrNull()
        if (oldCoordinates == null) {
            transaction {
                Locations.insert {
                    it[Locations.login] = login
                    it[Locations.cord1] = cord1
                    it[Locations.cord2] = cord2
                }
            }
        } else {
            transaction {
                Locations.update({ Locations.login eq login }) {
                    it[by.kolovaitis.users.Locations.cord1] = cord1
                    it[by.kolovaitis.users.Locations.cord2] = cord2
                }
            }
        }
    }

}

class NotInfoException(s: String) : Exception(s) {

}
