package by.kolovaitis.users

import org.jetbrains.exposed.sql.Table

object Watched: Table() {
    val id= Watched.integer("id").primaryKey().autoIncrement()
    val login = Watched.text("login")
    val login_anket = Watched.text("login_anket")
}