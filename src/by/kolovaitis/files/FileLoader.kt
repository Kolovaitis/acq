package by.kolovaitis.files

import java.io.File
import java.io.FileNotFoundException

object FileLoader {
    private val parentDirectory: File by lazy {
        val file = File("./files")
        if (!file.exists()) {
            file.mkdir()
        }
        file
    }

    fun loadFile(fileName: String, bytes: ByteArray) {
        val file = File(parentDirectory, fileName)
        if (!file.exists()) {
            file.createNewFile()
        }
        val outputStream = file.outputStream()
        outputStream.write(bytes)
        outputStream.close()
    }

    fun getFile(login: String): File {
        return parentDirectory.listFiles().find { it.nameWithoutExtension.equals(login) } ?: throw FileNotFoundException()
    }
}